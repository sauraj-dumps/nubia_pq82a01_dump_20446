#!/system/bin/sh
# Copyright (c) 2009, ZTE . All rights reserved.
#History:
#when       who         what, where, why
#--------   ----        ---------------------------------------------------
#2022-6-17 10310975   capture ipc log
##########################################
chmod 775 /data/local/vendor_logs/kernel/
rm -rf /data/local/vendor_logs/kernel/qup_uart_rx.txt
rm -rf /data/local/vendor_logs/kernel/qup_uart_tx.txt
rm -rf /data/local/vendor_logs/kernel/qup_uart_misc.txt
rm -rf /data/local/vendor_logs/kernel/qup_uart_pwr.txt

mount -t debugfs none /sys/kernel/debug

echo "Start to cat ipc log ..."
BOARD=$(getprop ro.board.platform)

if [ "$BOARD" = "taro" ]; then
    cat /sys/kernel/debug/ipc_logging/894000.qcom,qup_uart_rx/log_cont > /data/local/vendor_logs/kernel/qup_uart_rx.txt &
    cat /sys/kernel/debug/ipc_logging/894000.qcom,qup_uart_tx/log_cont > /data/local/vendor_logs/kernel/qup_uart_tx.txt &
    cat /sys/kernel/debug/ipc_logging/894000.qcom,qup_uart_misc/log_cont > /data/local/vendor_logs/kernel/qup_uart_misc.txt &
    cat /sys/kernel/debug/ipc_logging/894000.qcom,qup_uart_pwr/log_cont > /data/local/vendor_logs/kernel/qup_uart_pwr.txt &
    cat /sys/kernel/debug/ipc_logging/894000.qcom,qup_uart_irqstatus/log_cont > /data/local/vendor_logs/kernel/qup_uart_irqstatus.txt &

elif [ "$BOARD" = "lahaina" ]; then
    cat /sys/kernel/debug/ipc_logging/890000.qcom,qup_uart_rx/log_cont > /data/local/vendor_logs/kernel/qup_uart_rx.txt &
    cat /sys/kernel/debug/ipc_logging/890000.qcom,qup_uart_tx/log_cont > /data/local/vendor_logs/kernel/qup_uart_tx.txt &
    cat /sys/kernel/debug/ipc_logging/890000.qcom,qup_uart_misc/log_cont > /data/local/vendor_logs/kernel/qup_uart_misc.txt &
    cat /sys/kernel/debug/ipc_logging/890000.qcom,qup_uart_pwr/log_cont > /data/local/vendor_logs/kernel/qup_uart_pwr.txt &
    cat /sys/kernel/debug/ipc_logging/890000.qcom,qup_uart_irqstatus/log_cont > /data/local/vendor_logs/kernel/qup_uart_irqstatus.txt &

elif [ "$BOARD" = "kona" ]; then
    cat /sys/kernel/debug/ipc_logging/998000.qcom,qup_uart_rx/log_cont > /data/local/vendor_logs/kernel/qup_uart_rx.txt &
    cat /sys/kernel/debug/ipc_logging/998000.qcom,qup_uart_tx/log_cont > /data/local/vendor_logs/kernel/qup_uart_tx.txt &
    cat /sys/kernel/debug/ipc_logging/998000.qcom,qup_uart_misc/log_cont > /data/local/vendor_logs/kernel/qup_uart_misc.txt &
    cat /sys/kernel/debug/ipc_logging/998000.qcom,qup_uart_pwr/log_cont > /data/local/vendor_logs/kernel/qup_uart_pwr.txt &
    cat /sys/kernel/debug/ipc_logging/998000.qcom,qup_uart_irqstatus/log_cont > /data/local/vendor_logs/kernel/qup_uart_irqstatus.txt &

else
    echo "something is wrong with platform"

fi
