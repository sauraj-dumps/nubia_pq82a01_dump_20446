#!/bin/bash

cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat vendor/lib64/libCOSNet_spatial_qnn_quantized.so.* 2>/dev/null >> vendor/lib64/libCOSNet_spatial_qnn_quantized.so
rm -f vendor/lib64/libCOSNet_spatial_qnn_quantized.so.* 2>/dev/null
cat vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin.* 2>/dev/null >> vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin
rm -f vendor/bin/COSNet_spatial_8bit_quantized.serialized.bin.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat vendor_bootimg/06_dtbdump_Qualcomm_Technologies,_Inc._KalamaP_SoC.dtb.* 2>/dev/null >> vendor_bootimg/06_dtbdump_Qualcomm_Technologies,_Inc._KalamaP_SoC.dtb
rm -f vendor_bootimg/06_dtbdump_Qualcomm_Technologies,_Inc._KalamaP_SoC.dtb.* 2>/dev/null
cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
cat bootRE/boot.elf.* 2>/dev/null >> bootRE/boot.elf
rm -f bootRE/boot.elf.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat system/system/lib64/libzteFaceRecognitionModels.so.* 2>/dev/null >> system/system/lib64/libzteFaceRecognitionModels.so
rm -f system/system/lib64/libzteFaceRecognitionModels.so.* 2>/dev/null
cat system/system/app/ThemeResource_Nubia/ThemeResource_Nubia.apk.* 2>/dev/null >> system/system/app/ThemeResource_Nubia/ThemeResource_Nubia.apk
rm -f system/system/app/ThemeResource_Nubia/ThemeResource_Nubia.apk.* 2>/dev/null
cat system/system/priv-app/SuperWallpaper_Earth/SuperWallpaper_Earth.apk.* 2>/dev/null >> system/system/priv-app/SuperWallpaper_Earth/SuperWallpaper_Earth.apk
rm -f system/system/priv-app/SuperWallpaper_Earth/SuperWallpaper_Earth.apk.* 2>/dev/null
cat system/system/priv-app/Gallery_Nubia_MFV/Gallery_Nubia_MFV.apk.* 2>/dev/null >> system/system/priv-app/Gallery_Nubia_MFV/Gallery_Nubia_MFV.apk
rm -f system/system/priv-app/Gallery_Nubia_MFV/Gallery_Nubia_MFV.apk.* 2>/dev/null
cat system/system/priv-app/GameSpace-nubia/GameSpace-nubia.apk.* 2>/dev/null >> system/system/priv-app/GameSpace-nubia/GameSpace-nubia.apk
rm -f system/system/priv-app/GameSpace-nubia/GameSpace-nubia.apk.* 2>/dev/null
cat system/system/priv-app/NubiaDiyaod/NubiaDiyaod.apk.* 2>/dev/null >> system/system/priv-app/NubiaDiyaod/NubiaDiyaod.apk
rm -f system/system/priv-app/NubiaDiyaod/NubiaDiyaod.apk.* 2>/dev/null
cat system/system/priv-app/Nubia_PhotoEditor/Nubia_PhotoEditor.apk.* 2>/dev/null >> system/system/priv-app/Nubia_PhotoEditor/Nubia_PhotoEditor.apk
rm -f system/system/priv-app/Nubia_PhotoEditor/Nubia_PhotoEditor.apk.* 2>/dev/null
cat system/system/priv-app/NubiaCamera/NubiaCamera.apk.* 2>/dev/null >> system/system/priv-app/NubiaCamera/NubiaCamera.apk
rm -f system/system/priv-app/NubiaCamera/NubiaCamera.apk.* 2>/dev/null
cat system/system/etc/faceclassifier/faceclassifier.dlc.* 2>/dev/null >> system/system/etc/faceclassifier/faceclassifier.dlc
rm -f system/system/etc/faceclassifier/faceclassifier.dlc.* 2>/dev/null
cat system/system/etc/pictureclassifier/PictureClassifier.tflite.* 2>/dev/null >> system/system/etc/pictureclassifier/PictureClassifier.tflite
rm -f system/system/etc/pictureclassifier/PictureClassifier.tflite.* 2>/dev/null
cat system_ext/partner-app/NubiaBaiduSearch/NubiaBaiduSearch.apk.* 2>/dev/null >> system_ext/partner-app/NubiaBaiduSearch/NubiaBaiduSearch.apk
rm -f system_ext/partner-app/NubiaBaiduSearch/NubiaBaiduSearch.apk.* 2>/dev/null
cat system_ext/partner-app/NubiaGDMap/NubiaGDMap.apk.* 2>/dev/null >> system_ext/partner-app/NubiaGDMap/NubiaGDMap.apk
rm -f system_ext/partner-app/NubiaGDMap/NubiaGDMap.apk.* 2>/dev/null
cat system_ext/partner-app/Weibo/Weibo.apk.* 2>/dev/null >> system_ext/partner-app/Weibo/Weibo.apk
rm -f system_ext/partner-app/Weibo/Weibo.apk.* 2>/dev/null
cat system_ext/partner-app/NubiaUCBrowser/NubiaUCBrowser.apk.* 2>/dev/null >> system_ext/partner-app/NubiaUCBrowser/NubiaUCBrowser.apk
rm -f system_ext/partner-app/NubiaUCBrowser/NubiaUCBrowser.apk.* 2>/dev/null
cat system_ext/partner-app/SmartHome/SmartHome.apk.* 2>/dev/null >> system_ext/partner-app/SmartHome/SmartHome.apk
rm -f system_ext/partner-app/SmartHome/SmartHome.apk.* 2>/dev/null
cat system_ext/partner-app/NubiaCtrip/NubiaCtrip.apk.* 2>/dev/null >> system_ext/partner-app/NubiaCtrip/NubiaCtrip.apk
rm -f system_ext/partner-app/NubiaCtrip/NubiaCtrip.apk.* 2>/dev/null
cat system_ext/partner-app/NubiaHuya/NubiaHuya.apk.* 2>/dev/null >> system_ext/partner-app/NubiaHuya/NubiaHuya.apk
rm -f system_ext/partner-app/NubiaHuya/NubiaHuya.apk.* 2>/dev/null
cat system_ext/priv-app/Settings_MFV/Settings_MFV.apk.* 2>/dev/null >> system_ext/priv-app/Settings_MFV/Settings_MFV.apk
rm -f system_ext/priv-app/Settings_MFV/Settings_MFV.apk.* 2>/dev/null
cat system_ext/priv-app/SystemUI_MFV/SystemUI_MFV.apk.* 2>/dev/null >> system_ext/priv-app/SystemUI_MFV/SystemUI_MFV.apk
rm -f system_ext/priv-app/SystemUI_MFV/SystemUI_MFV.apk.* 2>/dev/null
