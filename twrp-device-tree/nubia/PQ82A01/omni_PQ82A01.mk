#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from PQ82A01 device
$(call inherit-product, device/nubia/PQ82A01/device.mk)

PRODUCT_DEVICE := PQ82A01
PRODUCT_NAME := omni_PQ82A01
PRODUCT_BRAND := nubia
PRODUCT_MODEL := NX711J
PRODUCT_MANUFACTURER := nubia

PRODUCT_GMS_CLIENTID_BASE := android-nubia

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="CN_PQ82A01-user 13 TKQ1.221013.002 20230112.005443 release-keys"

BUILD_FINGERPRINT := nubia/CN_PQ82A01/PQ82A01:13/TKQ1.221013.002/20230112.005443:user/release-keys
