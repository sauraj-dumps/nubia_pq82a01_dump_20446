#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_PQ82A01.mk

COMMON_LUNCH_CHOICES := \
    lineage_PQ82A01-user \
    lineage_PQ82A01-userdebug \
    lineage_PQ82A01-eng
