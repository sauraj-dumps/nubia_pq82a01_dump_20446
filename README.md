## qssi-user 13 TKQ1.221013.002 20230112.114842 release-keys
- Manufacturer: nubia
- Platform: kalama
- Codename: PQ82A01
- Brand: nubia
- Flavor: qssi-user
- Release Version: 13
- Kernel Version: 5.15.41
- Id: TKQ1.221013.002
- Incremental: 20230112.114842
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
true
- Locale: zh-CN
- Screen Density: undefined
- Fingerprint: nubia/CN_PQ82A01/PQ82A01:13/TKQ1.221013.002/20230112.005443:user/release-keys
- OTA version: 
- Branch: qssi-user-13-TKQ1.221013.002-20230112.114842-release-keys
- Repo: nubia_pq82a01_dump_20446
