#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_PQ82A01.mk

COMMON_LUNCH_CHOICES := \
    omni_PQ82A01-user \
    omni_PQ82A01-userdebug \
    omni_PQ82A01-eng
