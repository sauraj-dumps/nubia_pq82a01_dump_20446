#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_PQ82A01-user
add_lunch_combo omni_PQ82A01-userdebug
add_lunch_combo omni_PQ82A01-eng
